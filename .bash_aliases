# set stty options
stty ispeed 921600 > /dev/null 2>&1
stty ospeed 921600 > /dev/null 2>&1
stty eof '^Q'
stty start '^A'
stty stop '^S'
stty susp '^T'

# set Dir aliases
alias hm='cd'
alias up='cd ..'
alias rt='cd /'
alias bk='cd $OLDPWD'

alias mkdir='mkdir -p'
alias rmdir='rm -R'
alias mvdir='mv'
alias cpdir='cp -R'

# set Util aliases
alias sst='\ss'
alias ss='sudo -s'
alias sv='sudo -v'

alias cpa='cp -a'
alias cpadir='cp -aR'

alias gctl='loginctl'
alias hctl='hostnamectl'
alias jctl='journalctl'
alias lctl='localectl'
alias sctl='systemctl'
alias tctl='timedatectl'

alias firewall='ssh admin@10.10.1.1'
alias switch='ssh admin@10.10.1.11'
alias wifiap='ssh admin@10.10.1.21'

alias df='\df -hl'
alias dfs='\df -hl | grep -v "/var/lib/docker" | (read h; echo "$h"; sort -V)'
alias dfz='\df -hl -T -x"squashfs" -x"tmpfs" | grep -v "/var/lib/docker" | (read h; echo "$h"; sort -V)'
alias dft='\df -hl -T -x"squashfs" -x"zfs" | grep -v "/var/lib/docker" | (read h; echo "$h"; sort -V)'
alias dfl='\df -hl -T -x"tmpfs" -x"zfs" | grep -v "/var/lib/docker" | (read h; echo "$h"; sort -V)'
alias du='\du -h'
alias dua='sudo du -h --max-depth=1 --apparent-size 2> /dev/null | sort -hr'
alias dub='sudo du -h --max-depth=1 2> /dev/null | sort -hr'

alias cpu5='ps aux | (read h; echo "$h"; sort -nr -k 3) | head -6 | less -X -S -E'
alias mem5='ps aux | (read h; echo "$h"; sort -nr -k 4) | head -6 | less -X -S -E'

alias hlink='ln'
alias slink='ln -s'

alias ssa='service --status-all'
alias nli='sudo netstat -tulpn | grep LISTEN'
alias cte='crontab -e'
alias scte='sudo crontab -e'

alias ut='uptime -p'
alias nf='neofetch --source ~/.config/neofetch/logo_ubuntu_sm'
alias sxo='sudo sshfs am@xo:/var/log /var/log/xo'
#alias sxp='sudo sshfs am@xp:/var/log /var/log/xp'
alias cp-uc='sudo rsync -vaz /etc/unbound/unbound.conf.d/unbound.conf root@xo:/etc/unbound/unbound.conf.d'

alias info='pinfo'
alias ibu='pinfo binutils'
alias icu='pinfo coreutils'
alias idu='pinfo diffutils'
alias ifu='pinfo find'

alias ucr='sudo unbound-control reload'
alias ucc='unbound-checkconf'
alias scc='testparm'

alias st-cli='speedtest'
alias st-ip3='iperf3 -c iperf3.server.fqdn'

alias kmon='kmon -c c0c0c0'
alias plz='sudo $(fc -ln -1)'

# set APT aliases
if [ -f /usr/bin/apt ]; then
  alias update='sudo apt update'
  alias upgrade='sudo apt update && sudo apt dist-upgrade'
  alias clean='sudo apt autoremove && sudo apt clean'
  alias install='sudo apt install'
fi

# set Script aliases
alias dcs='/home/am/bash/docker-create-start.sh $1'
alias dcu='/home/am/bash/docker-compose-up.sh $1'

alias crypt-dir='/home/am/bash/crypt-dir.sh $1 $2'
alias crypt-file='/home/am/bash/crypt-file.sh $1 $2'

alias tmx='/home/am/bash/tmux-start.sh'
alias wlc='/home/am/bash/welcome.sh'


alias jobname='jobn=$(jobs -p | tail -1); ps -p $jobn -o comm='
alias joblist='jobs | wc -l'

# set Safetynets
alias rm='rm -I --preserve-root'

alias mv='mv -i'
alias cp='cp -i'
alias ln='ln -i'

alias chmod='chmod --preserve-root'
alias chown='chown --preserve-root'
alias chgrp='chgrp --preserve-root'

# set Special keybindings
# normally use -x (bashreload requires special binding)
alias bashreload='source ~/.bashrc'
bind '"\C-p"':"\"bashreload\C-m\""
bind '"\C-n"':"\"wlc\C-m\""

#bind -x '"\C-p":source ~/.bashrc'
#bind -x '"\C-n":/home/am/bash/welcome.sh'
bind -x '"\C-r":fg'

bind -r "\C-l"
bind -r "\C-y"

# set MANPATH (for snap man pages)
# export MANPATH=":/snap/tmux-non-dead/current/share/man:/snap/nano/current/share/man:/snap/htop/current/usr/local/share/man:/snap/nmap/current/share/man"
