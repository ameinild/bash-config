# Global settings
set -g mode-keys emacs
set -g status-keys emacs
set -g xterm-keys on
set -g default-terminal "screen-256color"
# set -g default-terminal "xterm-256color"

# User keys
set -s user-keys[0] "\e[1;5D"
set -s user-keys[1] "\e[1;5C"
#set -s user-keys[2] "\e[5~"
#set -s user-keys[3] "\e[6~"

# Set prefix key to C-x
unbind C-b
set -g prefix C-x
bind C-x send-prefix

# Reset old commands
unbind ,
unbind &
unbind w
unbind s
unbind '$'
unbind '"'
unbind '%'
unbind !
unbind q
unbind :
unbind x
unbind c
unbind n
unbind r
unbind l
unbind PageUp
unbind C-z
unbind -T copy-mode Space
unbind -T copy-mode C-Up
unbind -T copy-mode C-Down

# unbind -T copy-mode Home
# unbind -T copy-mode End
# unbind -T copy-mode C-a
# unbind -T copy-mode C-s

# Window commands
bind q new-window
bind w command-prompt 'rename-window %%'
bind e kill-window

# Session commands
bind a choose-session
bind s command-prompt 'rename-session %%'
bind -n C-d detach-client

# Pane commands
bind "'" split-window -h
bind - split-window -v
bind x break-pane
bind c kill-pane
bind n display-panes
bind y setw synchronize-panes

# Control commands
bind h copy-mode
bind PageUp copy-mode -eu
bind ½ paste-buffer
bind r command-prompt

# Copy Mode basic commands
bind -T copy-mode User0 send-keys -X start-of-line
bind -T copy-mode User1 send-keys -X end-of-line
bind -T copy-mode M-Home send-keys -X history-top
bind -T copy-mode M-End send-keys -X history-bottom
bind -T copy-mode C-a send-keys -X begin-selection
bind -T copy-mode C-s send-keys -X stop-selection
bind -T copy-mode C-q send-keys -X rectangle-toggle
bind -T copy-mode C-o send-keys -X copy-selection
bind -T copy-mode C-l send-keys -X copy-line

# Copy Mode advanced commands
bind -T copy-mode C-g send-keys -X previous-matching-bracket
bind -T copy-mode C-b send-keys -X next-matching-bracket

# Copy Mode search commands
bind -T copy-mode C-f command-prompt -p "(search up)" "send -X search-backward \"%%%\""
bind -T copy-mode C-v command-prompt -p "(search down)" "send -X search-forward \"%%%\""
bind -T copy-mode M-n send-keys -X search-reverse
bind -T copy-mode M-v send-keys -X search-again
bind -T copy-mode C-w send-keys -X cursor-right \; send-keys -X select-word
bind -T copy-mode C-e send-keys -X begin-selection \; send-keys -X end-of-line

# Shift+arrows to switch windows
bind -n C-Up previous-window
bind -n C-Down next-window

# Alt+arrows to switch panes
bind -n S-Left select-pane -L
bind -n S-Right select-pane -R
bind -n S-Up select-pane -U
bind -n S-Down select-pane -D

# Mouse mode
# setw -g mouse on

# Window options
set -g allow-rename off
set -g pane-border-style 'fg=colour245'
set -g pane-active-border-style 'fg=colour250'
set -g remain-on-exit on

# Screensaver & lock
set -g lock-after-time 300
set -g lock-command "unimatrix -a -s 96 ; vlock"
bind l lock-session

# Status bar theme
set -g status-interval 1
set -g status-justify 'centre'
set -g status-left " #[fg=colour6]#(echo $LSEP$LSEP) #[fg=colour2]#S #[fg=colour6]#(echo $LSEP) #[fg=colour250]#(echo $WHATDISTRO | awk -F'( )+' '{print $1}') #[fg=colour3]#(echo $WHATDISTRO | awk -F'( )+' '{print $2,$3}') #[fg=colour6]#(echo $LSEP) #[fg=colour250]#(echo $TERM2) #[fg=colour3]#(tmux display -p '#{pane_width}x#{pane_height}') #[fg=colour6]#(echo $LSEP) #[fg=colour250]#(echo $WHATSHELL | awk -F'( )+' '{print $1}') #[fg=colour3]#(echo $WHATSHELL | awk -F'( )+' '{print $2}') #[fg=colour6]#(echo $LSEP)"
set -g status-left-length 100
set -g status-right "#[fg=colour6]#(echo $RSEP) #[fg=colour250]cpu #[fg=colour3]#(cpustat4.sh)% #[fg=colour6]#(echo $RSEP) #[fg=colour250]mem #[fg=colour3]#(freemem2.sh) #[fg=colour6]#(echo $RSEP) #[fg=colour250]proc #[fg=colour3]#(cat /proc/loadavg | awk -F'( |/)+' '{print $4,$5}' | tr ' ' '/') #[fg=colour6]#(echo $RSEP) #[fg=colour250]load #[fg=colour3]#(cat /proc/loadavg | awk -F'( )+' '{print $1,$2,$3}') #[fg=colour6]#(echo $RSEP) #[fg=colour250]up #[fg=colour3]#(uptime2.sh) #[fg=colour6]#(echo $RSEP$RSEP) "
set -g status-right-length 100
set -g status-style 'fg=colour250,bg=colour8'

# Set synchronize visibility
set -g window-status-format '#{?#{==:#I,0},#[fg=colour1],#[fg=colour4]}[#I]#[fg=colour253,nobold]#W#[fg=colour3]#F#{?pane_synchronized,S,}#[fg=colour250]'
set -g window-status-current-format '#{?#{==:#I,0},#[fg=colour9],#[fg=colour12]}[#I]#[fg=colour15,nobold]#W#[fg=colour3]#F#{?pane_synchronized,S,}#[fg=colour250]'

# Easy config reload
bind p source-file ~/.tmux.conf \; display-message "~/.tmux.conf reloaded"
