# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# If not running interactively, don't do anything
case $- in
  *i*) ;;
    *) return;;
esac

# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.

if [ -f ~/.bash_aliases ]
then
  . ~/.bash_aliases
fi

# Export more variables to use with tmux
# These only run when tmux is not running
# tmux_running=$(tmux ls | grep 'created' | wc -l)
if [[ -z $TMUX ]]
then
  export TERM2=$TERM
  export LSEP=$(echo -e "\u2572")
  export RSEP=$(echo -e "\u2571")
fi

# Set PATH if running for the first time
[[ -z $WHATDISTRO ]] && export PATH="$PATH:$HOME/bash:$HOME/python"

export WHATDISTRO=$(~/bash/whatdistro.sh)
export WHATSHELL=$(~/bash/whatshell.sh | awk -F'(' '{print $1}')

# Attach to or create default tmux session (if not already running)
# Only from xterm-256color and not local Linux terminal
[[ $TERM == "xterm-256color" ]] && [[ $(pinky | grep 172.17 | wc -l) == 0 ]] && [[ $(hostname) == "xb" ]] && tmx

clear

# Print welcome message if not running in tmux
[[ -z $TMUX ]] &&  ~/bash/welcome.sh

# Set pane title to the current shell when running inside tmux
# [[ -n $TMUX ]] && tmux selectp -T "$(~/bash/whatshell.sh)"
# [[ -n $TMUX ]] && export tmuxtestshell=$(~/bash/whatshell.sh)

# Don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
export HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# After each command, save and reload history
export PROMPT_COMMAND="history -a; history -c; history -r; $PROMPT_COMMAND"

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=1000
HISTFILESIZE=2000

# Check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# If set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
#shopt -s globstar

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "${debian_chroot:-}" ] && [ -r /etc/debian_chroot ]
then
  debian_chroot=$(cat /etc/debian_chroot)
fi

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
  xterm-color|*-256color) color_prompt=yes;;
esac

# uncomment for a colored prompt, if the terminal has the capability; turned
# off by default to not distract the user: the focus in a terminal window
# should be on the output of commands, not on the prompt
force_color_prompt=yes

if [ -n "$force_color_prompt" ]
then
  if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null
  then
    # We have color support; assume it's compliant with Ecma-48
    # (ISO/IEC-6429). (Lack of such support is extremely rare, and such
    # a case would tend to support setf rather than setaf.)
    color_prompt=yes
  else
    color_prompt=
  fi
fi

if [ "$color_prompt" = yes ]
then
  PS1='${debian_chroot:+($debian_chroot)}\[\e[00;37m\]\342\225\255\342\224\200[\[\e[01;32m\]\u\[\e[00;37m\]@\[\e[01;32m\]\h\[\e[00;37m\]]\[\e[01;33m\]$(pinky | grep $(whoami) | tail -n +2 | wc -l)\[\e[00;37m\]:\[\e[01;33m\]$(if [[ $PWD == $HOME* ]]; then echo -n "~"; fi)\$\[\e[00;37m\]]\342\224\200[\[\e[01;34m\]$(stat -c '%U' .)\[\e[00;37m\]\302\246\[\e[01;34m\]$PWD\[\e[00;37m\]]\[\e[01;33m\]$(ls -Al | grep '^d' | wc -l)\[\e[00;37m\]:\[\e[01;33m\]$(ls -Ap | grep -v / | wc -l)\[\e[00;37m\]]$(if [[ $(jobs | wc -l) -gt 0 ]]; then echo -n "\342\224\200[\[\e[1;35m\]$(jobname)\[\e[00;37m\]]\[\e[01;33m\]$(joblist)\[\e[00;37m\]:\[\e[01;33m\]+\[\e[00;37m\]]"; fi)$(if [[ "$(git rev-parse --is-inside-work-tree 2>/dev/null)" == "true" ]]; then echo -n "\342\224\200[\[\e[1;38;5;210m\]$(git branch --show-current)\[\e[00;37m\]]\[\e[01;33m\]$(git rev-list HEAD --count)\[\e[00;37m\]:\[\e[01;33m\]\342\217\247\[\e[00;37m\]]"; fi)\n\[\e[00;37m\]\342\225\260\342\224\200\342\236\244\[\e[0m\] '
else
  PS1='${debian_chroot:+($debian_chroot)}[\u@\h]$(pinky | grep $(whoami) | tail -n +2 | wc -l):\$]-[$(stat -c '%U' .)\302\246\w]$(ls -Al | grep '^d' | wc -l):$(ls -Ap | grep -v / | wc -l)] '
fi

PS2=' \342\224\210\342\236\245\[\e[0m\] '
PS3=' \342\224\210\342\235\224\[\e[0m\] '

# Set 'man' colors
if [ "$color_prompt" = yes ]
then
  man() {
  env \
  LESS_TERMCAP_mb=$'\e[01;31m' \
  LESS_TERMCAP_md=$'\e[01;31m' \
  LESS_TERMCAP_me=$'\e[0m' \
  LESS_TERMCAP_se=$'\e[0m' \
  LESS_TERMCAP_so=$'\e[01;44;33m' \
  LESS_TERMCAP_ue=$'\e[0m' \
  LESS_TERMCAP_us=$'\e[01;32m' \
  man "$@"
  }
fi

unset color_prompt force_color_prompt

# If this is an xterm set the title to user@host:dir
case "$TERM" in
  xterm*|rxvt*)
    PS1="\[\e]0;${debian_chroot:+($debian_chroot)}[\u@\h]-[\w]\a\]$PS1"
    ;;
  *)
    ;;
esac

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]
then
  test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
  alias ls='LC_COLLATE=C ls --color=auto'
  alias dir='dir --color=auto'
  alias vdir='vdir --color=auto'

  alias grep='grep --color=auto'
  alias fgrep='fgrep --color=auto'
  alias egrep='egrep --color=auto'
fi

# colored GCC warnings and errors
#export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

# some more ls aliases
alias ll='ls -alhG'
alias ld="ls -AlhG --color=yes | grep '^d' | less -REX"
alias lf="ls -AlhG --color=yes | grep -v '^d' | less -REX"
alias la='ls -A'
alias l='ls -CF'

# Add an "alert" alias for long running commands.  Use like so:
#   sleep 10; alert
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix
then
  if [ -f /usr/share/bash-completion/bash_completion ]
  then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]
  then
    . /etc/bash_completion
  fi
fi
